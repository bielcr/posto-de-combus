/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package posto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aluno
 */
public class Modificar {

    public void modFunc(String nome, String cpf, int idade) {
        try {
            Connection c = Conexao.getConection();
            PreparedStatement ps = c.prepareStatement("update sc_posto.funcionario set nome = ?, idade = ? where cpf = ?");
            ps.setString(1, nome);
            ps.setString(3, cpf);
            ps.setInt(2, idade);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Modificar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void modGer(String nome, String cpf, int registro) {
        try {
            Connection c = Conexao.getConection();
            PreparedStatement ps = c.prepareStatement("update sc_posto.registro set nome = ?, registro = ? where cpf = ?");
            ps.setString(1, nome);
            ps.setString(3, cpf);
            ps.setInt(2, registro);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Modificar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

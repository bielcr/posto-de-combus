/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package posto;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JList;

/**
 *
 * @author aluno
 */
public class Listar {

    public void listar(JList listagem) {
        try {
            listagem.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection c = Conexao.getConection();
            String sql = "Select * from sc_posto.bomba_de_combustivel";
            PreparedStatement ps = c.prepareStatement(sql);
            ResultSet rs = ps.executeQuery(sql);
            while (rs.next()) {
                dfm.addElement(rs.getString("codBomba"));
            }
            listagem.setModel(dfm);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Listar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void listarFunc(JList listagem) {
        try {
            listagem.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection c = Conexao.getConection();
            String sql = "Select * from sc_posto.funcionario";
            PreparedStatement ps = c.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                dfm.addElement(rs.getString("cpf"));
            }
            listagem.setModel(dfm);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Listar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void selecaoF (JComboBox combo){
        try {
            DefaultComboBoxModel m = new DefaultComboBoxModel();
            Connection c = Conexao.getConection();
            PreparedStatement ps = c.prepareStatement("Select * from sc_posto.funcionario");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Funcionario f = new Funcionario(rs.getString("cpf"), rs.getString("nome"));
                m.addElement(f);
            }
            combo.setModel(m);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Listar.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package posto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aluno
 */
public class Insercao {
    public void inserirBomba(String codBomba, String senha, String tipo, String preco, String mangueiras){
        try {
            Connection c = Conexao.getConection();
            PreparedStatement ps = c.prepareStatement("insert into sc_posto.bomba_de_combustivel(codigo_bomba, senha, tipo_combustivel, preco, num_Mangueira)"
                    + "values(?, ?, ?, ?, ?)");
            ps.setString(1, codBomba);
            ps.setString(2, senha);
            ps.setString(3, tipo);
            ps.setString(4, preco);
            ps.setString(5, mangueiras);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void inserirFuncionario(String cpf, String nome, int idade){
        try {
            Connection c = Conexao.getConection();
            PreparedStatement ps = c.prepareStatement("insert into sc_posto.funcionario(nome, idade, cpf) values(?, ?, ?)");
            ps.setString(1, nome);
            ps.setString(3, cpf);
            ps.setInt(2, idade);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void inserirGerente(String cpf, String nome, int registro){
        try {
            Connection c = Conexao.getConection();
            PreparedStatement ps = c.prepareStatement("insert into sc_posto.gerente(nome, cpf, registro) values(?, ?, ?)");
            ps.setString(1, nome);
            ps.setString(2, cpf);
            ps.setInt(3, registro);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
